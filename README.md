# quiz-project

quiz project from BBT
node.js echo word `"Hello DevOps Quiz#2!!"`

__1. Project with docker-compose on vm is located at `~/quiz/quiz-project/`__

    Example of result 
    ~/quiz/quiz-project/compose$ `docker-compose up`

    node-quiz-local | Hello DevOps Quiz#2!!

![](./compose-result.png)

__2. Automate CI/CD using GitLab with .gitlab-ci.yml__

    1. build and test on Gitlab share runner
    2. deploy by gitlab-runner on VM
    3. Result after container run successfully can be check at `cat /tmp/quiz-result` on VM
    
![](./pipeline_result.png)

__3. System diagram__

![](./bbt-quiz-diagram.png)
